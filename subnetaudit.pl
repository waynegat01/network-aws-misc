#sudo yum install perl-JSON
#sudo yum install perl-Data-Dumper
#sudo rm /usr/share/vim/vim81/indent.vim
#
#touch subnetaudit.pl
#chmod 755 ./subnetaudit.pl

#This is a test

#!/usr/bin/env perl
#use warnings;

use JSON;
use Data::Dumper;



my $rcmd = `aws ec2 describe-regions --output json --query 'Regions[*].RegionName'`;
my $rdata = decode_json $rcmd;

my $vpcs;

foreach my $region ( @{$rdata} ) {
	next unless ($region =~ /^us-/ );

	my $cmd = `aws ec2 describe-vpcs --region $region --output json --query "Vpcs[*].[CidrBlock, VpcId,Tags[?Key=='Name'].Value]" 2>&1`;
	
	next unless (defined $cmd);
	next if ($cmd eq '');
	next if ($cmd =~ /not authorized/);
	

	my $data = decode_json $cmd;
	
	
	foreach my $entry ( @{$data} ) {
		$vpcs->{id}->{${$entry}[1]}->{cidr} = ${$entry}[0];
		$vpcs->{id}->{${$entry}[1]}->{name} = ${${$entry}[2]}[0];
	}
}

print Dumper $vpcs;	

foreach my $region ( @{$rdata} ) {
	next unless ($region =~ /^us-/ );

	my $cmd = `aws ec2 describe-route-tables --region $region --output json --query "RouteTables[*].[RouteTableId, Associations[*].[SubnetId]]" 2>&1`;
	
	next unless (defined $cmd);
	next if ($cmd eq '');
	next if ($cmd =~ /not authorized/);
	

	my $data = decode_json $cmd;
	
	
	foreach my $entry ( @{$data} ) {
		my $routetid = ${$entry}[0];
		foreach my $entry2 ( @{${$entry}[1]} ) {
			my $subnetid = ${$entry2}[0];
			next unless (defined($subnetid));
			$vpcs->{subnettoroute}->{$subnetid} = $routetid;
		}
	}
}



foreach my $region ( @{$rdata} ) {
	next unless ($region =~ /^us-/ );
	
	my $cmd = '';
	$cmd = `aws ec2 describe-subnets --region $region --output json --query "Subnets[*].[CidrBlock, SubnetId, VpcId, Tags[?Key=='Name'].Value]" 2>&1`;
	
	next unless (defined($cmd));
	next if ($cmd eq '');
	next if ($cmd =~ /not authorized/);
	
	my $data = decode_json $cmd;
	

	foreach my $entry ( @{$data} ) {
		print ${$entry}[0].','.${$entry}[1].','.${${$entry}[3]}[0].','.${$entry}[2].','.$region.','.$vpcs->{id}->{${$entry}[2]}->{cidr}.','.$vpcs->{id}->{${$entry}[2]}->{name}.','.$vpcs->{subnettoroute}->{${$entry}[1]}."\n";
	}
}	
