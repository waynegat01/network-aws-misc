#!/usr/bin/env bash
# Requires: awscli (http://aws.amazon.com/cli/)
# Prints out a list of all security groups and their settings, just for quickly auditing it.

# Your AWS credentials
#if [ -z ${AWS_ACCESS_KEY_ID} ]; then
#    export AWS_ACCESS_KEY_ID='***'
#    export AWS_SECRET_ACCESS_KEY='***'
#fi

#wget https://raw.githubusercontent.com/dryoni/aws-tools/main/sg-tool/sg.py
#sudo python3 -m pip install dnspython
#sudo python3 -m pip install netaddr

#./awssecuritygrpaudit2_cloudshell.sh >> auditoutput.txt

chmod 755 ./sg.py

# Want to do this for all regions...
#REGIONS=(`/usr/local/bin/aws ec2 describe-regions --query 'Regions[*].RegionName' --region us-east-1 --output text | tr '\t' '\n'`)
REGIONS="us-east-1"
for REGION in ${REGIONS[*]}; do
    echo "=> $REGION"

	# Grab all the security group info for this region in one call.
	GFILE='/tmp/aws-sec-groups'
	/usr/local/bin/aws ec2 describe-security-groups --region $REGION --output text --no-cli-pager > $GFILE

	
    # Loop over each line of the file and parse it.
    old_IFS=$IFS; IFS=$'\n'
    cat $GFILE | while read line
    do
        case $line in
            # Header
			
            SECURITYGROUPS*)
                SID=(`echo $line | awk -F '\t' '{print $3}'`)
                GNAME=(`echo $line | awk -F '\t' '{print $4}'`)
				echo ""
					echo ""
					echo ""
					echo " <<<<   Running SG for Group $GNAME - $SID >>>>"
					echo ""
					echo "python3 sg.py $SID"
					echo ""
				python3 sg.py $SID
				if [ $? -ne 0 ]; then
					echo "ERROR! Running SG for Group $GNAME - $SID  Again"
					python3 sg.py $SID
					if [ $? -ne 0 ]; then
						echo "ERROR! Running SG for Group $GNAME - $SID  Again"
						python3 sg.py $SID
						if [ $? -ne 0 ]; then
							echo "ERROR! Running SG for Group $GNAME - $SID  Again"
							python3 sg.py $SID
							if [ $? -ne 0 ]; then
								echo "ERROR! Running SG for Group $GNAME - $SID  Again"
								python3 sg.py $SID
								if [ $? -ne 0 ]; then
									echo "python3 sg.py $SID >> auditoutput.txt" >> auditfailed.sh
								fi
							fi
						fi
					fi
				fi
                ;;
        esac
		#echo ""
    done
    IFS=$old_IFS

    # Clean up
    rm $GFILE
done



echo ""